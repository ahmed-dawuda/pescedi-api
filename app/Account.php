<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function depositWithdraw(){
        return $this->hasMany('App\DepositWithdrawTransaction')->orderBy('created_at', 'DESC');
    }
    
    public function leagueTransactions(){
        return $this->hasMany('App\LeagueTransaction')->orderBy('created_at', 'DESC');
    }
}
