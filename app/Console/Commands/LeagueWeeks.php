<?php

namespace App\Console\Commands;

use App\GlobalHelpers\Leagues\MatchDaySimulation;
use App\Jobs\SimulateLeague;
use Illuminate\Console\Command;

class LeagueWeeks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'league:simulate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Simulate leagues whose expiry date is today at 12:00:00';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $simulation = new MatchDaySimulation();
        $leagues = $simulation->leagues_weekly_Matches();
        foreach($leagues as $key => $league){
            SimulateLeague::dispatch($league, $key);
        }

    }
}
