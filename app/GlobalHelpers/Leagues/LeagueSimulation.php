<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 10/23/2017
 * Time: 7:38 PM
 */

namespace App\GlobalHelpers\Leagues;


use App\Account;
use App\League;
use App\LeagueFixture;
use App\LeagueTable;
use App\LeagueTransaction;
use App\LeagueWinner;
use App\UserTeam;
use App\Week;

class LeagueSimulation
{
    public $weeks;
    public $league_id;

    function __construct($league, $league_id)
    {
//        dd($league);
        $this->weeks = $league;
        $this->league_id = $league_id;
    }

    public function simulate(){
        foreach ($this->weeks as $key => $week){
//            dd($key);
            $league_week = new Week();
            $league_week->name = $key;
            $league_week->league_id = $this->league_id;
            $league_week->save();

//            dd($league_week->id);
            $this->simulate_week($week, $league_week->id);
        }

        $this->finish();
    }

    public function simulate_week($fixtures, $league_week_id){
        foreach ($fixtures as $fixture){
            $league_fixture = $this->simulate_fixture($fixture);
            $league_fixture->week_id = $league_week_id;
            $league_fixture->save();
//            dd($league_fixture);
            $this->store($league_fixture);
        }
    }

    public function simulate_fixture($fixture){
        $common_chances = random_int(1,7);
//        dd($common_chances);
        $hs = 0;
        $as = 0;
//        dd($fixture[1]["attack"]);
        for($chance = 1; $chance <= $common_chances; $chance++){
            $attack = random_int(1,$fixture[0]["attack"]);
//            dd($attack);
            $defense = random_int(1, $fixture[1]["midfield"]) + random_int(1, $fixture[1]["defense"]);
//            dd($defense);
            $net = $attack - $defense;

            $hs = $net > 0 ? $hs + 1: $hs + 0;
//            dd($net,$hs);

            $attack = random_int(1, $fixture[1]["attack"]);
//            dd($attack);
            $defense = random_int(1, $fixture[0]["defense"]) + random_int(1, $fixture[0]["midfield"]);
//            dd($defense);
            $net = $attack - $defense;
            $as = $net > 0 ? $as + 1: $as + 0;
//            dd($net, $as);
        }
//        dd($fixture[0]['name'].": ".$hs.", ".$fixture[1]['name'].": ".$as);
//        dd($fixture[0]["id"]);
        $league_fixture = new LeagueFixture();
        $league_fixture->h_id = $fixture[0]["id"];
        $league_fixture->a_id = $fixture[1]["id"];
        $league_fixture->h_score = $hs;
        $league_fixture->a_score = $as;
//        dd($league_fixture);
        return $league_fixture;
    }

    public function store(LeagueFixture $result){
        //retrieving the teams' standings
        $league_table_home = LeagueTable::where('user_team_id', $result->h_id)->where('league_id',$this->league_id)->get()->first();
        $league_table_away = LeagueTable::where('user_team_id', $result->a_id)->where('league_id',$this->league_id)->get()->first();

        //getting user teams
        $home = UserTeam::find($result->h_id);
        $away = UserTeam::find($result->a_id);

        $home->goals += $result->h_score;
        $home->concede += $result->a_score;
        $away->goals += $result->a_score;
        $away->concede += $result->h_score;
        $home->matches++;
        $away->matches++;

        //updating teams goals
//        dd($result);
        $league_table_away->g_f += $result->a_score;
//        dd($result, $league_table_away);
        $league_table_away->g_a += $result->h_score;
//        dd($result, $league_table_away);

        $league_table_away->g_d = $league_table_away->g_f - $league_table_away->g_a;
//        dd($result, $league_table_away);

        $league_table_home->g_f += $result->h_score;
        $league_table_home->g_a += $result->a_score;
        $league_table_home->g_d = $league_table_home->g_f - $league_table_home->g_a;
//        dd($result, $league_table_home);

        //updating the number of played matches
        $league_table_home->played += 1;
        $league_table_away->played += 1;

//        dd($result, $league_table_away, $league_table_home);

        //updating their points
        if($result->a_score > $result->h_score){
            $league_table_away->won += 1;
            $league_table_away->pts += 3;
            $league_table_home->loss += 1;

            $away->won++;
            $away->points += 0.002;
            $home->loss++;

        }else if($result->h_score > $result->a_score){
            $league_table_home->won += 1;
            $league_table_home->pts += 3;
            $league_table_away->loss += 1;

            $home->won++;
            $away->loss++;
            $home->points += 0.002;
        }else{
            $league_table_home->pts += 1;
            $league_table_away->pts += 1;
            $league_table_home->drawn += 1;
            $league_table_away->drawn += 1;
            $home->points += 0.001;
            $away->points += 0.001;
        }

        //calculating win percentages
        $home->win_percent = $home->won / $home->matches * 100;
        $home->draw_percent = ($home->matches - $home->won - $home->loss) / $home->matches * 100;
        $away->win_percent = $away->won / $away->matches * 100;
        $away->draw_percent = ($away->matches - $away->loss - $away->won) / $away->matches * 100;
//        dd($result->toArray(), $league_table_home->toArray(), $league_table_away->toArray() );

        //updating those entries
        $league_table_away->save();
        $league_table_home->save();

        $home->update();
        $away->update();
//        dd($league_table_home->toArray(), $league_table_away->toArray());

    }

    public function finish(){
        $table = LeagueTable::where('league_id', $this->league_id)
            ->orderBy('pts', 'DESC')
            ->orderBy('g_d', 'DESC')
            ->orderBy('g_f', 'DESC')
            ->orderBy('g_a', 'ASC')->get();

        $winner = new LeagueWinner();
        $winner->user_team_id = $table->first()->user_team_id;
        $winner->league_id = $this->league_id;
        $winner->save();

        $league = League::find($this->league_id);
        $league->completed = true;
        $league->save();

        //calculating earningsgfs

        $ratios = [];
        for($i = 1; $i <= $table->count(); $i++){
            $ratios[] = 3.31 + ($i - 1) * 1.764;
        }

//        dd(count($ratios));
        $ratios = array_reverse($ratios);
        $sum = array_sum($ratios);
        $earning = [];

        $pescedi = Account::find(1);
        $pescedi->balance += $league->participants * $league->entry_fee * 0.02;
        $pescedi->update();

        $amount = $league->participants * $league->entry_fee * 0.98;

        for($j = 0; $j < $table->count(); $j++){
            $earning[$j] = ($ratios[$j] / $sum) * $amount;
        }
        $count = 0;
        foreach ($table as $item){
            $transaction = new LeagueTransaction();
            $transaction->type = 'earning';
            $transaction->amount = $earning[$count];
            $account = UserTeam::find($item->user_team_id)->user->account;
            $transaction->account_id = $account->id;
            $account->balance += $transaction->amount;
            $account->update();
            $transaction->league = $league->name;
            $transaction->transaction_id = time()."".random_int(100, 999);
            $transaction->save();
            $item->earning = $transaction->amount;
            $item->update();
            $count++;
        }
    }
}