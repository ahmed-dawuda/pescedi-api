<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 10/23/2017
 * Time: 12:32 PM
 */

namespace App\GlobalHelpers\Leagues;


use App\League;
use App\LeagueTable;
use App\Participation;
use App\UserTeam;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class MatchDaySimulation
{
    public $weeks;
    public $no_weeks;
    public $no_of_fixtures_in_week;


    public function activeLeagues(){
        $leagues = League::where('completed', false)->where('expiry','<=', Carbon::now())->get();
        return $leagues;
    }

    public function participants(League $league){
        $participants = $league->teams;
        if($participants->count() % 2 == 1){

            $team = UserTeam::find(1);
            $table_row = new LeagueTable();
            $table_row->user_team_id = $team->id;
            $table_row->league_id = $league->id;
            $table_row->save();

            $partake = new Participation();
            $partake->user_team_id = $team->id;
            $partake->league_id = $league->id;
            $partake->save();

            $participants->push($team);
            $league->participants += 1;
            $league->update();
        }
        return $participants;
    }


    public function weeks($fixtures){
        $explored = array(array());
        $weeks = array();
        $c = 0;
        for($week = 0; $week < $this->no_weeks; $week++){
            $weeks["week_".($week+1)][] = $fixtures[$c];
            $explored[$week][] = $fixtures[$c][0]["id"];
            $explored[$week][] = $fixtures[$c][1]["id"];
            $c++;
        }


        for($week = 0; $week < $this->no_weeks; $week++){

            foreach($fixtures as $f){

                if(in_array($f[0]["id"], $explored[$week]) || in_array($f[1]["id"], $explored[$week])){

                }else{
                    $weeks["week_".($week+1)][] = $f;
                    $explored[$week][] = $f[0]["id"];
                    $explored[$week][] = $f[1]["id"];
                    unset($f);
                }
            }
        }
        return $weeks;
    }

    public function makeFixtures($participants){
        $this->no_of_fixtures_in_week = (int) count($participants)/2;
        $this->no_weeks = 2 * (count($participants) - 1);
        $participants = $participants->toArray();
        $fixtures = array();

        for($i = 0; $i < count($participants); $i++){
            for($j = 0; $j < count($participants); $j++){
                if($i == $j){
                    continue;
                }else{
                    $fixtures[] = array($participants[$i], $participants[$j]);
                }
            }
        }
        return $fixtures;
    }

    public function leagues_weekly_Matches(){
        $data = [];
       $leagues = $this->activeLeagues();
        foreach($leagues as $league){
            $participants = $this->participants($league);
            $fixtures = $this->makeFixtures($participants);
            $weeks = $this->weeks($fixtures);
            $data[$league->id] = $weeks;
        }
        return $data;
    }
}