<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 9/24/2017
 * Time: 11:13 AM
 */

namespace App\GlobalHelpers\SetFixtures;
use App\GlobalHelpers\SetFixtures\GenerateOdds;
use App\GlobalHelpers\SetFixtures\Team;
use App\TeamFixture;

class Fixture
{
    public $id;
    public $home;
    public $away;
//    public $score;
//    public $status;
    public $odds;
    public $head2head;
    public $prediction;

    function __construct(Team $home,Team $away)
    {
        $this->id = random_int(10000,99999);
//        $this->score = $score;
        $this->home = $home;
        $this->away = $away;
//        $this->status = $status;
        $this->odds = new GenerateOdds($home, $away);
        $total = TeamFixture::where('home_id',$home->id)->where('away_id', $away->id)->get()->count() +
            TeamFixture::where('home_id', $away->id)->where('away_id', $home->id)->get()->count();

        $home_times = TeamFixture::where('home_id', $home->id)->where('away_id', $away->id)->where('home_score', '>', 'away_score')->get()->count() +
            TeamFixture::where('away_id', $home->id)->where('home_id', $away->id)->where('away_score','>', 'home_score')->get()->count();

        $away_times = TeamFixture::where('home_id', $away->id)->where('away_id', $home->id)->where('home_score','>','away_score')->get()->count() +
            TeamFixture::where('away_id', $away->id)->where('home_id', $home->id)->where('away_score', '>', 'home_score')->get()->count();
        $this->head2head = ['total'=>$total, 'home'=> $home_times, 'away'=> $away_times];
        $h = (int) ($home->win_percent / 10);
        $a = (int) ($away->win_percent / 10);
        $high = 0;
        $low = 0;
        $sub = 1;

        if($h > $a){
            $high = $h;
            $low  = $a;
        }else{
            $high = $a;
            $low = $h;
        }
        if($high > 4){
            $sub = 3;
        }else{
            $sub = 1;
        }

        $h -= $sub;
        $a -= $sub;
       $h = $h < 0 ? 0 : $h;
        $a = $a < 0 ? 0 : $a;
        $this->prediction = ['home'=> $h, 'away'=> $a];
    }
}