<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 10/1/2017
 * Time: 1:41 PM
 */

namespace App\GlobalHelpers\SetFixtures;


class GenerateOdds
{
    public $one;
    public $two;
    public $draw;
    public $hg;
    public $ag;
    public $onex;
    public $twox;

    public function __construct($home, $away){
        $homeWin = ($home->win_percent + (100 - ($away->draw_percent + $away->win_percent)))/2;
        $awayWin = ($away->win_percent + (100 - ($home->draw_percent + $home->win_percent)))/2;
        $draw = ($home->draw_percent + $away->draw_percent)/2;
        $this->one = round((1 - (($homeWin + 4)/100))/(($homeWin + 4)/100),2) + 1;
        $this->two = round((1 - (($awayWin + 4)/100) )/(($awayWin + 4)/100),2) + 1;
        $this->draw = round((1 - (($draw + 2)/100))/(($draw + 2)/100), 2) + 1;
        $this->onex = ($this->one - 1) + random_int(0, 100)/100;
        $this->twox = ($this->two - 1) + random_int(0, 100)/100;
        $this->onex = $this->onex < 1 ? 1 + random_int(5,15)/100 : $this->onex;
        $this->twox = $this->twox < 1 ? 1 + random_int(5,15)/100 : $this->twox;

    }
}