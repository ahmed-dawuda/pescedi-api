<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 10/5/2017
 * Time: 10:55 PM
 */

namespace App\GlobalHelpers\SetFixtures;


class Result
{
    public $id;
    public $bet;
    public $score;
    public $won;

    public function __construct($id, $bet, $score, $won)
    {
        $this->id = $id;
        $this->bet = $bet;
        $this->score = $score;
        $this->won = $won;
    }
}