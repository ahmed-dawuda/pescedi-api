<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 9/24/2017
 * Time: 11:14 AM
 */

namespace App\GlobalHelpers\SetFixtures;
use App\Team as TeamModel;
use App\TeamFixture;

class Team
{
    public $id;
    public $name;
    public $description;
    public $attack;
    public $midfield;
    public $defense;
    public $matches;
    public $loss;
    public $won;
    public $goals;
    public $concede;
    public $draw_percent;
    public $win_percent;
    public $current_form;
//    public $turns;
    public $position;
    public $last_four;


    public function __construct( TeamModel $team, $take ){
        $this->current_form = array();
        $this->id = $team->id;
        $this->name = $team->name;
        $this->description = $team->description;
        $this->attack = $team->attack;
        $this->midfield = $team->midfield;
        $this->defense = $team->defense;
        $this->matches = $team->matches;
        $this->loss = $team->loss;
        $this->won = $team->won;
        $this->goals = $team->goals;
        $this->concede = $team->concede;
        $this->win_percent = round($team->win_percent * 100, 2);
        $this->draw_percent = round($team->draw_percent * 100,2);
        $forms = $team->forms;//->take($take);
        foreach($forms as $form){
            $this->current_form[] = $form->status;
        }
        $table = TeamModel::orderBy('win_percent', 'DESC')
            ->orderBy('won', 'DESC')
            ->orderBy('draw_percent', 'DESC')->get();

        $position = 1;
        foreach($table as $table){
            if($table->id == $this->id){
                $this->position = $position;
                break;
            }
            $position++;
        }
        $this->last_four = array();
        $last4 = TeamFixture::where('home_id', $this->id)->orWhere('away_id', $this->id)->orderBy('created_at', 'DESC')->take(4)->get();
        foreach($last4 as $match){
            if($match->home_id == $this->id){
                $form = '';
                if($match->home_score > $match->away_score){
                    $form = 'W';
                }else if($match->home_score == $match->away_score){
                    $form = 'D';
                }else if($match->home_score < $match->away_score){
                    $form = 'L';
                }
                $opponent = TeamModel::find($match->away_id)->name;
                $this->last_four[] = array('team'=> "vs ".$opponent, 'form'=> $form);
            }

            if($match->away_id == $this->id){
                $form = '';
                if($match->away_score > $match->home_score){
                    $form = 'W';
                }else if($match->away_score == $match->home_score){
                    $form = 'D';
                }else if($match->away_score < $match->home_score){
                    $form = 'L';
                }
                $opponent = TeamModel::find($match->home_id)->name;
                $this->last_four[] = array('team'=> $opponent." vs", 'form'=> $form);
            }
        }
    }
}