<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 9/24/2017
 * Time: 2:59 PM
 */

namespace App\GlobalHelpers\Simulation;
use App\Form;
use App\Jobs\UpdateTeamForm;
use App\Setup;
use App\Team;
use App\GlobalHelpers\SetFixtures\Team as SimTeam;
use App\TeamFixture;

class Simulate
{
    public $chance;
    public $home;
    public $away;
    public $score;
    public $bet;
    public $id;
    public $won;
    public $slip_id;

    public function __construct($bet, $slip_id)
    {
        $this->home = Team::find($bet['home']);
        $this->away = Team::find($bet['away']);
        $this->chances = random_int(3,10);
        $this->score = array('home'=> 0, 'away'=> 0);
        $this->bet = $bet['bet'];
        $this->id = $bet['id'];
        $this->slip_id = $slip_id;
//        $this->log('home: '.$this->home->name.', away: '.$this->away->name);
    }

    /**
     * @return $this
     */
    public function sim(){
        $default_chance = (int) ( $this->home->draw_percent * 10 + $this->away->draw_percent * 10);
//        $this->log('initial default chances: ' . $default_chance);
        if($default_chance > 6) $default_chance = 6;
//        $this->log('final default chances: ' . $default_chance);
        for($i = 1; $i <= $default_chance; $i++){
            $this->attack(0);
            $this->attack(1);
        }
        $this->simB();

        $this->goalTest();
        return $this;
    }

    public function simB(){

        $home_chance = (int) ($this->home->win_percent * 10);
        $away_chance = (int) ($this->away->win_percent * 10);

//        $this->log('home_chances: '.$home_chance.' away_chances: '.$away_chance);

        for($i = 1; $i <= $home_chance; $i++){
            $this->attack(0);
        }
        for($j = 1; $j <= $away_chance; $j++){
            $this->attack(1);
        }
    }

    public function attack($attacker){
        if($attacker == 0){
            $home_attack = rand(1, $this->home->attack);
            $away_defense = rand(1, $this->away->midfield) + rand(1, $this->away->defense);
            $net = $home_attack - $away_defense;
            if($net > 0){
                $this->score['home']++;
            }
//            $this->log('home attacks with: '.$home_attack .', away defense with: '.$away_defense);

        }else{
            $away_attack = rand(1, $this->away->attack);
            $home_defense = rand(1, $this->home->midfield) + rand(1, $this->home->defense);
            $net = $away_attack - $home_defense;
            if($net > 0){
                $this->score['away']++;
            }
//            $this->log('away attacks with: '.$away_attack .', home defense with: '.$home_defense);
        }
    }

    public function goalTest(){
        switch ($this->bet['type']){
            case 'one':
                if($this->score['home'] > $this->score['away']) $this->won = true;
                else $this->won = false;
            break;
            case 'two':
                if($this->score['away'] > $this->score['home']) $this->won = true;
                else $this->won = false;
            break;
            case 'onex':
                if($this->score['home'] >= $this->score['away']) $this->won = true;
                else $this->won = false;
                break;
            case 'twox':
                if($this->score['away'] >= $this->score['home']) $this->won = true;
                else $this->won = false;
                break;
            case 'draw':
                if($this->score['away'] == $this->score['home']) $this->won = true;
                else $this->won = false;
                break;
            default:
                $this->won = 'unknown';

        }
        UpdateTeamForm::dispatch($this->home, $this->away, $this->score, $this->bet, $this->won, $this->slip_id);
    }
    
    public function updateForm(){
       
    }

    public function result(){
        return array(
            'id' => $this->id,
            'bet' => $this->bet,
            'score' => $this->score,
            'won' => $this->won
        );
    }

    public function log($message){
        $log = new Setup();
        $log->user_id = 1;
        $log->name = $message;
        $log->save();
    }
}