<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 11/11/2017
 * Time: 5:06 PM
 */

namespace App\GlobalHelpers\SlipWrapper;


use App\Slip;
use App\Team;

class SingleSlip
{
    public $id;
    public $odd;
    public $stake;
    public $staked_on;
    public $fixtures = [];
    public $won;


    function __construct(Slip $slip)
    {
        $this->id = $slip->slip_id;
        $this->odd = $slip->odd;
        $this->stake = $slip->stake;
        $this->staked_on = $slip->created_at;
        $this->won = $slip->status == 1? true : false;

        $fixtures = $slip->fixtures;

        foreach($fixtures as $fixture){
            $home = Team::find($fixture->home_id)->name;
            $away = Team::find($fixture->away_id)->name;
            $option = '';
            switch ($fixture->bet_type) {
                case 'one':
                    $option = '1';
                    break;
                case 'two':
                    $option = '2';
                    break;
                case 'onex':
                    $option = '1 / X';
                    break;
                case 'twox':
                    $option = '2 / X';
                    break;
                case 'draw':
                    $option = 'X';
                    break;
            }
            $this->fixtures[] = array('home'=> $home, 'away'=> $away, 'bet'=> $option, 'status'=> $fixture->status ? true : false, 'odd'=> $fixture->odd);
        }
    }
}