<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 9/22/2017
 * Time: 6:20 PM
 */

namespace App\GlobalHelpers;


class StatusCodes
{
    public static $vErrors = 422;
    public static $success = 200;
    public static $created = 201;
    public static $unauthorized = 401;
}