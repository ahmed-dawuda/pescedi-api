<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 11/19/2017
 * Time: 5:41 PM
 */

namespace App\GlobalHelpers\Wrappers;


use App\Account;
use App\DepositWithdrawTransaction;

class AccountWrapper
{
    function __construct(Account $ac)
    {
        $this->account = $ac;
        $account = clone $ac;
        $this->withdrawals = $account->depositWithdraw->where('type','withdrawal')->count();
        $this->deposits = $account->depositWithdraw->where('type','deposits')->count();
        $this->earnings = $account->leagueTransactions->count();
        $this->league_transactions = $account->leagueTransactions->take(10);
        $this->depwith_transaction = $account->depositWithdraw->take(10);
        $this->slip_transactions = $account->user->slips->take(10);
    }
}