<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 11/14/2017
 * Time: 5:01 PM
 */

namespace App\GlobalHelpers\Wrappers;


use App\Week;

class FixtureWrapper
{
    public $fixtures = array();
    
    function __construct($league_id)
    {
        $weeks = Week::where('league_id', $league_id)->get();
        foreach($weeks as $week){
            $this->fixtures[] = new WeekWrapper($week);
        }
    }
}