<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 11/14/2017
 * Time: 4:31 PM
 */

namespace App\GlobalHelpers\Wrappers;


use App\LeagueTable;
use Illuminate\Support\Facades\Auth;

class LeagueTableWrapper
{
    public $standings = array();

    function __construct($league_id)
    {
        $table = LeagueTable::where('league_id', $league_id)
            ->orderBy('pts', 'DESC')
            ->orderBy('g_d', 'DESC')
            ->orderBy('g_f', 'DESC')
            ->orderBy('g_a', 'ASC')->get();
        $position = 1;
        $auth_team_id = Auth::user()->user_team->id;
        
        foreach ($table as $row){
            $this->standings[] = new TableRowWrapper($row, $position, $auth_team_id);
            $position++;
        }

    }
}