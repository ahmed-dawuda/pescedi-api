<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 11/13/2017
 * Time: 2:15 PM
 */

namespace App\GlobalHelpers\Wrappers;


use App\League;
use App\User;

class LeagueWrapper
{

    function __construct(League $league)
    {
        $this->id = $league->id;
        $this->name = $league->name;
        $this->description = $league->description;
        $this->owner = User::find($league->owner)->user_team->name;
        $this->expiry = $league->expiry;
        $this->entry_fee = $league->entry_fee;
        $this->no_of_participants = $league->participants;
        $this->completed = $league->completed;
    }
}