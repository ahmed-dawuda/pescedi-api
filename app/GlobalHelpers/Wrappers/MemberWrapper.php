<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 11/14/2017
 * Time: 6:08 PM
 */

namespace App\GlobalHelpers\Wrappers;


use App\League;

class MemberWrapper
{
    public $members = array();

    function __construct($league_id)
    {
        $league = League::find($league_id);
        if($league){
            $members = $league->teams;
            foreach ($members as $member){
                $image_link = asset('images/default.png');
                $image = $member->image;
                $image_link = count($image) ? asset('storage/uploads/team/'.$image->name) : $image_link;
                $this->members[] = [
                    'image'=> $image_link,
                    'wins' => $member->win_percent,
                    'draws'=> $member->draw_percent,
                    'attack' => $member->attack,
                    'midfield' => $member->midfield,
                    'defense' => $member->defense,
                    'name' => $member->name
                ];
            }
        }
    }
}