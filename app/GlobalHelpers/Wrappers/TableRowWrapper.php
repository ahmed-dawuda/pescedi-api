<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 11/14/2017
 * Time: 4:44 PM
 */

namespace App\GlobalHelpers\Wrappers;


use App\LeagueTable;
use App\UserTeam;

class TableRowWrapper
{
    function __construct(LeagueTable $row, $position, $id)
    {
//        dd($row);
        $this->team = UserTeam::find($row->user_team_id)->name;
        $this->isUser = $id == $row->user_team_id ? true : false;
        $this->position = $position;
        $this->MP = $row->played;
        $this->W = $row->won;
        $this->D = $row->drawn;
        $this->L = $row->loss;
        $this->GF = $row->g_f;
        $this->GD = $row->g_d;
        $this->GA = $row->g_a;
        $this->PTS = $row->pts;
        $this->earning = $row->earning;
        
    }
}