<?php
/**
 * Created by PhpStorm.
 * User: Kofi Bae's Boo
 * Date: 11/14/2017
 * Time: 5:04 PM
 */

namespace App\GlobalHelpers\Wrappers;


use App\UserTeam;
use App\Week;
use Illuminate\Support\Facades\Auth;

class WeekWrapper
{
   public $name;
    public $matches = array();

    function __construct(Week $week)
    {
        $this->name = str_replace('_', ' ',$week->name);
        $matches = $week->leagueFixtures;
        $user_team = Auth::user()->user_team;
        foreach($matches as $match){
            $home = UserTeam::find($match->h_id);
            $away = UserTeam::find($match->a_id);
            $this->matches[] = [
                'home'=> $home->name,
                'away'=> $away->name,
                'score'=> ['home'=> $match->h_score, 'away'=> $match->a_score],
                'isUser' => ['home'=> $home->id == $user_team->id, 'away'=> $away->id == $user_team->id]
            ];
        }
    }
}