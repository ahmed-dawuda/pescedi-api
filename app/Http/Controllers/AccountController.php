<?php

namespace App\Http\Controllers;

use App\GlobalHelpers\StatusCodes;
use App\GlobalHelpers\Wrappers\AccountWrapper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function details(Request $request){
        $account = Auth::user()->account;
        $details = new AccountWrapper($account);
        return response()->json(['status'=> true, 'message'=> 'account details', 'data'=> $details], StatusCodes::$success);
    }
}
