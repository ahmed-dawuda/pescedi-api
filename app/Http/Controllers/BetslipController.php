<?php

namespace App\Http\Controllers;

use App\Account;
use App\GlobalHelpers\GenerateToken;
use App\GlobalHelpers\Simulation\Simulate;
use App\GlobalHelpers\SlipWrapper\SingleSlip;
use App\GlobalHelpers\StatusCodes;
use App\Slip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BetslipController extends Controller
{

    public function simulate(Request $request){
        $betResult = array();
        $won = true;
        $odd = 1;
        $account = Auth::user()->account;
        if($account->balance < $request->all()['stake']){
            return response()->json(['status'=> false, 'message'=> 'You dont have enough funds to stake this bet', 'data'=> null], StatusCodes::$vErrors);
        }
        $slip = new Slip();
        $slip->user_id = Auth::user()->id;
        $slip->slip_id = GenerateToken::getToken(5);
        $slip->save();

      foreach($request->all()['slip'] as $fixture){
          $bet = new Simulate($fixture, $slip->id);
          $betResult[] = $bet->sim()->result();
          $won = $won && $betResult[count($betResult) - 1]['won'];
          $odd = $odd * $betResult[count($betResult) - 1]['bet']['odd'];
      }
        $slip->odd = $odd;
        $slip->stake = $request->all()['stake'];
        $slip->status = $won;
        $slip->save();

        $account->balance -= $slip->stake;
        $pes = Account::find(1);
        $pes->balance += $slip->stake;
        $pes->update();
        
        if ($slip->status){
            $account->balance += ($slip->stake * $odd);
        }
        $account->update();

        $betResult[] = ['won'=>$won,'stake'=> $request->all()['stake'], 'total_odd'=>$odd, 'gain'=> $odd * $request->all()['stake']];

        return response()->json(['status'=> true,'message'=> 'Betslip simulation result', 'data'=> $betResult],StatusCodes::$success);
    }

    public function slips(Request $request){
        $slips = Auth::user()->slips;
        $result = array();
        foreach ($slips as $slip){
            $result[] = new SingleSlip($slip);
        }
        return response()->json(['status'=> true, 'message'=> 'User slips', 'data'=> collect($result)->chunk(3)], StatusCodes::$success);

    }
}
