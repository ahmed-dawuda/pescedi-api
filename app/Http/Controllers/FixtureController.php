<?php

namespace App\Http\Controllers;

use App\GlobalHelpers\SlipWrapper\SingleSlip;
use App\GlobalHelpers\StatusCodes;
use App\TeamFixture;
use Illuminate\Http\Request;
use App\GlobalHelpers\SetFixtures\Fixture;
use App\Team;
use App\GlobalHelpers\SetFixtures\Team as TeamWrapper;
use Illuminate\Support\Facades\Auth;

class FixtureController extends Controller
{
    /**
     * pair teams for fixtures
     * @param Request $request
     * @return array
     */
    public function fixtures(Request $request){

        $teams = Team::all();

        $wrappedTeams = array();

        foreach($teams as $team){

            $wrappedTeams[] = new TeamWrapper($team,4);

        }

        shuffle($wrappedTeams);

        $fixtures = array();

        for($i = 0; $i < count($wrappedTeams); $i++){

            for($j = $i + 1; $j < count($wrappedTeams); $j++){

                $fixtures[] = new Fixture($wrappedTeams[$i] , $wrappedTeams[$j]);

            }

        }

        shuffle($fixtures);

        return response()->json( ['status'=> true, 'message'=> 'Possible pairing of teams', 'data'=> collect($fixtures)->chunk(3), 'fix'=>$fixtures], StatusCodes::$success);

    }


    public function table(Request $request){
        $slips = Auth::user()->slips;
//        return $slips->first()->fixtures;
        $result = array();
        foreach ($slips as $slip){
            $result[] = new SingleSlip($slip);

        }
        return $result;
//        return $slips;
    }

}
