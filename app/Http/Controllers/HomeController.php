<?php

namespace App\Http\Controllers;
use App\GlobalHelpers\StatusCodes;
use App\League;
use App\Slip;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request){
        $pending = League::where('completed', false)->get()->count();
        $completed = League::where('completed', true)->get()->count();
        $bet = Slip::where('status', true)->get()->count();
        $maxwins = random_int(1000, 5000) + random_int(11, 99)/100;

        return response()->json(['status'=> true, 'message'=> 'Stats for home page',
            'data'=> ['betwins'=> $bet, 'leaguesWon'=> $completed, 'pendingLeagues'=> $pending, 'members' => User::all()->count(), 'maxwins'=> $maxwins]],
            StatusCodes::$success);
    }
}
