<?php

namespace App\Http\Controllers;

use App\Account;
use App\GlobalHelpers\StatusCodes;
use App\GlobalHelpers\Wrappers\FixtureWrapper;
use App\GlobalHelpers\Wrappers\LeagueTableWrapper;
use App\GlobalHelpers\Wrappers\LeagueWrapper;
use App\GlobalHelpers\Wrappers\MemberWrapper;
use App\League;
use App\LeagueTable;
use App\LeagueTransaction;
use App\Participation;
use App\UserTeam;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class LeagueController extends Controller
{
    public function create(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=> 'required|min:3',
            'description'=>'required',
            'entry_fee'=>'required|numeric',
            'expiry' => 'required|date',
        ]);

        if($validator->fails()){
            return response()->json(['status'=> false, 'message'=> 'Error in data provided', 'data'=> $validator->errors()],StatusCodes::$vErrors);
        }
        
        $data = $request->all();
        $league = new League();
        $league->name = ucfirst($data['name']);
        $league->description = ucfirst($data['description']);
        $league->entry_fee = $data['entry_fee'];
        $league->expiry = $data['expiry'];
        $league->owner = Auth::user()->id;
        $league->participants = 1;
        $league->save();

        $account = Auth::user()->account;
        if($account->balance < $league->entry_fee){
            $league->participants -= 1;
            $league->update();
            return response()->json(['status'=> false,
                'message'=> 'You dont have enough funds to join these league. League is created, but you can join later',
                'data'=> []], StatusCodes::$vErrors);
        }

        $account->balance -= $league->entry_fee;
        $account->update();
        $pes = Account::find(1);
        $pes->balance += $league->entry_fee;
        $pes->update();

        $transaction = new LeagueTransaction();
        $transaction->type = 'entry fee';
        $transaction->amount = $league->entry_fee;
        $transaction->account_id = $account->id;
        $transaction->league = $league->name;
        $transaction->transaction_id = time()."".random_int(100, 999);
        $transaction->save();

        //creating a new participation
        $participation = new Participation();
        $participation->user_team_id = Auth::user()->user_team->id;
        $participation->league_id = $league->id;
        $participation->save();

        //creating entry for this user within the league table
        $league_table_row = new LeagueTable();
        $league_table_row->user_team_id = Auth::user()->user_team->id;
        $league_table_row->league_id = $league->id;
        $league_table_row->save();

        return response()->json(['status'=>true, 'message'=> 'League created', 'data'=> []],StatusCodes::$created);
    }
    
    public function join(Request $request){

        $data = $request->all();

        $user_account = Auth::user()->account;

        //finding the league and incrementing its participants
        $league = League::find($data['league_id']);


        if ($league->completed) {
            return response()->json(['status'=> false, 'message'=> 'You cannot join a completed league', 'data'=> []], StatusCodes::$vErrors);
        }

        if ($league->entry_fee > $user_account->balance){
            return response()->json(['status'=> false,
                'message'=> 'You don\'t have enough funds to join this league. Consider depositing some funds into your account', 'data'=> []], StatusCodes::$vErrors);
        }

        $league->participants++;
        $league->update();

        $participation = new Participation();
        $participation->user_team_id = Auth::user()->user_team->id;
        $participation->league_id = $data['league_id'];
        $participation->save();


        //creating entry for this user within the league table
        $league_table_row = new LeagueTable();
        $league_table_row->user_team_id = Auth::user()->user_team->id;
        $league_table_row->league_id = $data['league_id'];
        $league_table_row->save();

        //deducting from the account
        $user_account->balance -= $league->entry_fee;
        $user_account->save();
        
        $pes = Account::find(1);
        $pes->balance += $league->entry_fee;
        $pes->update();

        //creating a league transaction
        $transaction = new LeagueTransaction();
        $transaction->type = 'entry fee';
        $transaction->amount = $league->entry_fee;
        $transaction->account_id = $user_account->id;
        $transaction->league = $league->name;
        $transaction->transaction_id = time()."".random_int(100, 999);
        $transaction->save();

        return response()->json(['status'=>true, 'message'=> 'league joined', 'data'=> null],StatusCodes::$success);
    }
    
    public function upcoming(Request $request){
        $leagues = Auth::user()->user_team->upcomingLeagues();
        return response()->json(['status'=> true, 'message'=> 'Upcoming leagues', 'data'=> $leagues], StatusCodes::$success);
    }

    public function completed(Request $request){
        $leagues = Auth::user()->user_team->completedLeagues();
        return response()->json(['status'=> true, 'message'=> 'Completed leagues', 'data'=> $leagues], StatusCodes::$success);
    }

    public function leave(Request $request){
        $league_id = $request->all()['league_id'];
        $league = League::find($league_id);

        if($league->completed) {
            return response()->json(['status'=> false, 'message'=> 'You cant leave already completed league', 'data'=> null],StatusCodes::$vErrors);
        }else{
            Participation::where('user_team_id', Auth::user()->user_team->id)->where('league_id', $league_id)->delete();
            $league->participants -= 1;
            $league->update();
            $account = Auth::user()->account;
            $account->balance += $league->entry_fee;
            $account->update();
            
            $pes = Account::find(1);
            $pes->balance -= $league->entry_fee;
            $pes->update();
            
            LeagueTable::where('user_team_id', Auth::user()->user_team->id)->where('league_id', $league->id)->delete();

            $transaction = new LeagueTransaction();
            $transaction->type = 'refund';
            $transaction->amount = $league->entry_fee;
            $transaction->account_id = $account->id;
            $transaction->league = $league->name;
            $transaction->transaction_id = time()."".random_int(100, 999);
            $transaction->save();

            return response()->json(['status'=> true, 'message'=> 'league left', 'data'=> null]);
        }
    }

    public function latest(Request $request){
        $leagues = Auth::user()->user_team->unJoinedLeaguesRepo()->get();
        return response()->json(['status'=> true, 'message'=> 'latest leagues', 'data'=> $leagues]);
    }

    public function nextPrev(Request $request, $current_id){
//        return $request->all();
        $leagues = Auth::user()->user_team->leagues();
        $response = null;
        if($request->type == 'completed'){
            $response = $leagues->where('completed', true)->orderBy('expiry', 'DESC');
        } else if($request->type == 'pending'){
            $response = $leagues->where('completed', false)->orderBy('expiry', 'ASC');
        } else if($request->type == 'latest'){
            $response = Auth::user()->user_team->unJoinedLeaguesRepo();
        }
        $league = null;
       if($request->direction == 'next'){
           $league = $response->where('leagues.id','>', $current_id)->first();
       } else if($request->direction == 'previous'){
           $league = $response->where('leagues.id', '<', $current_id)->orderBy('id', 'DESC')->first();
       }
        if($league != null){
            $fixtures = new FixtureWrapper($league->id);
            $table = new LeagueTableWrapper($league->id);
            $members = new MemberWrapper($league->id);
            $hasJoined = Participation::where('user_team_id', Auth::user()->user_team->id)->where('league_id', $league->id)->count() ? true : false;
            return response()->json(['status'=> true, 'message'=> 'league results', 'data'=> [
                'fixtures' => $fixtures->fixtures,
                'members'  => $members->members,
                'standings'=> $table->standings,
                'league'   => $league,
                'joined' => $hasJoined
            ]], StatusCodes::$success);
        }

        return response()->json(['status'=> false, 'message'=> 'No league', 'data'=> null], StatusCodes::$vErrors);
    }
}
