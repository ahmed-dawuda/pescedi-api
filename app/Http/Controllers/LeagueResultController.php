<?php

namespace App\Http\Controllers;

use App\GlobalHelpers\StatusCodes;
use App\GlobalHelpers\Wrappers\FixtureWrapper;
use App\GlobalHelpers\Wrappers\LeagueTableWrapper;
use App\GlobalHelpers\Wrappers\MemberWrapper;
use App\League;
use App\Participation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LeagueResultController extends Controller
{
    public function fixtures(Request $request, $league_id){
        $fixtures = new FixtureWrapper($league_id);
        return response()->json(['status'=> true, 'message'=> 'All Matches for league '.$league_id, 'data'=> $fixtures], StatusCodes::$success);
    }
    
    public function table(Request $request, $league_id){
        $table = new LeagueTableWrapper($league_id);
        return response()->json(['status'=> true, 'message'=> 'Table standing for league '.$league_id, 'data'=> $table], StatusCodes::$success);
    }

    public function members(Request $request, $league_id){
        $members = new MemberWrapper($league_id);
        return response()->json(['status'=> true, 'message'=> 'participants', 'data'=> $members], StatusCodes::$success);
    }

    public function results(Request $request, $league_id){
        $fixtures = new FixtureWrapper($league_id);
        $table = new LeagueTableWrapper($league_id);
        $members = new MemberWrapper($league_id);
        $hasJoined = Participation::where('user_team_id', Auth::user()->user_team->id)->where('league_id', $league_id)->count() ? true : false;
        return response()->json(['status'=> true, 'message'=> 'league results', 'data'=> [
            'fixtures' => $fixtures->fixtures,
            'members'  => $members->members,
            'standings'=> $table->standings,
            'league'   => League::find($league_id),
            'joined' => $hasJoined
        ]], StatusCodes::$success);
    }
}
