<?php

namespace App\Http\Controllers;

use App\GlobalHelpers\StatusCodes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;

class LoginController extends Controller
{
    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            'phone' => 'required|numeric|min:10',
            'password' => 'required|string|min:6'
        ]);

        if($validator->fails()){
            return response()->json(['status'=> false, 'message'=> 'error during data validation', 'data'=> $validator->errors()],StatusCodes::$vErrors);
        }

        $user = User::where( 'phone', $request->phone)->get()->first();

        if(!is_null($user) && Hash::check($request->password, $user->password)){
            return response()->json(['status'=>true, 'message'=> 'login successful', 'data'=>$user], StatusCodes::$success);
        }else{
            return response()->json(['status'=>false,'message'=>'Incorrect phone and password combination', 'data'=> $request->toArray()],StatusCodes::$vErrors);
        }
    }
}
