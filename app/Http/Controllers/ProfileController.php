<?php

namespace App\Http\Controllers;

use App\GlobalHelpers\StatusCodes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class ProfileController extends Controller
{
    public function update(Request $request){
        $validator = Validator::make($request->all(),[
           'fullname' => 'required',
            'email' => 'required|email',
            'username' => 'required|string|min:3|alpha_num',
            'phone' => 'required|string|numeric|min:10',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),StatusCodes::$vErrors);
        }

        $data = $request->all();

        if(count(User::where('username',$data['username'])->where('id','!=',Auth::user()->id)->get()) > 0){
            return response()->json(['status'=>false,'message'=> 'Someone else is using this username', 'data'=> null],StatusCodes::$vErrors);
        }

        if(count(User::where('email',$data['email'])->where('id','!=', Auth::user()->id)->get()) > 0){
            return response()->json(['status'=> false, 'message'=> 'Someone else is using this username', 'data'=> null],StatusCodes::$vErrors);
        }

        if(count(User::where('phone',$data['phone'])->where('id','!=',Auth::user()->id)->get()) > 0){
            return response()->json(['status'=> false, 'message'=> 'You cannot use this phone, someone else is using it'],StatusCodes::$vErrors);
        }


        $user = Auth::user();
        $user->fullname = $data['fullname'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->username = $data['username'];
        $user->profile = 1;
        $user->update();
        return response()->json(['status'=> false, 'message'=> 'profile updated successfully', 'data'=> null], StatusCodes::$success);
    }
}
