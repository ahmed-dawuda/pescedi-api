<?php

namespace App\Http\Controllers;

use App\Account;
use App\GlobalHelpers\GenerateToken;
use App\GlobalHelpers\StatusCodes;
use App\User;
use App\UserTeam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function register(Request $request){

        $validator = Validator::make($request->all(),[
            'fullname' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required|string|numeric|min:10|unique:users'
        ]);

        if($validator->fails()){
            return response()->json(['status'=> false, 'message'=> 'Errors in data provided', 'data'=> $validator->errors()],StatusCodes::$vErrors);
        }

        $user = User::create([
            'password' => bcrypt($request->password),
            'email' => $request->email,
            'phone' => $request->phone,
            'api_token' => GenerateToken::getToken(64),
            'username' => $request->username,
            'fullname' => $request->fullname,
            'team' => true
        ]);
        //creating account for user
        $account = new Account();
        $account->user_id = $user->id;
        $account->balance = 100;
        $account->save();

        //creating team for user
        $team = new UserTeam();
        $team->user_id = $user->id;
        $team->name = $request['username'];
        $team->save();

        return response()->json(['status'=>true,'data'=>[], 'message'=> 'User registered'],StatusCodes::$created);
    }
}
