<?php

namespace App\Http\Controllers;

use App\GlobalHelpers\StatusCodes;
use App\Setup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SetupController extends Controller
{
    public function settings(Request $request){

        $settings = Auth::user()->setup;
        if($settings){
            $settings->all = $request['all'];
            $settings->withinweek = $request['withinWeek'];
            $settings->lower = $request['lower10'];
            $settings->upper = $request['upper10'];
            $settings->anytime = $request['anytime'];
            $settings->canafford = $request['canafford'];
            $settings->update();
        }else{
            $settings = new Setup();
            $settings->all = $request['all'];
            $settings->withinweek = $request['withinWeek'];
            $settings->lower = $request['lower10'];
            $settings->upper = $request['upper10'];
            $settings->anytime = $request['anytime'];
            $settings->canafford = $request['canafford'];
            $settings->user_id = Auth::user()->id;
            $settings->save();
        }
        return response()->json(['status'=> true, 'message'=> 'Settings saved', 'data'=> null], StatusCodes::$created);
    }


    public function turn(){
        Auth::user()->setup->delete();
        return response()->json(['status'=> true, 'message'=> 'settings turned on'], StatusCodes::$success);
    }
}
