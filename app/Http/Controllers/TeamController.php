<?php

namespace App\Http\Controllers;

use App\GlobalHelpers\StatusCodes;
use App\Image;
use App\LeagueWinner;
use App\Setup;
use App\UserTeam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TeamController extends Controller
{
    public function myTeam(Request $request){
        $user_team = Auth::user()->user_team;

        $image = $user_team->image;

        $image_link = asset('images/default.png');

        if($image){
            $image_link = asset('storage/uploads/team/'.$image->first()->name);
        }

        $trophies = LeagueWinner::where('user_team_id', $user_team->id)->get()->count();
//
        $ranking = UserTeam::where('id', '>', 1)
            ->orderBy('win_percent', 'DESC')
            ->orderBy('draw_percent', 'DESC')
            ->orderBy('goals', 'DESC')
            ->orderBy('concede', 'ASC')->get()->first();
        $bot = Auth::user()->setup;

        return response()->json(['status'=>true, 'message'=> Auth::user()->username."'s team", 'data'=>
            ['team'=> Auth::user()->user_team, 'profile'=> $image_link, 'trophies'=> $trophies, 'rank'=>
                ['name'=> $ranking ? $ranking->name : ''], 'bot'=> $bot ? true : false]],StatusCodes::$success);
    }

    public function create(Request $request){
        $data = $request->all();
        $all = $data['attack'] + $data['midfield'] + $data['defense'] + $data['points'];

        if($all !== 100) return response()->json(['status'=>false, 'message'=> 'This data is not acceptable', 'data'=> $data], StatusCodes::$vErrors);

        $team = new UserTeam();
        $team->name = $data['name'];
        $team->attack = $data['attack'];
        $team->defense = $data['defense'];
        $team->midfield = $data['midfield'];
        $team->points = $data['points'];
        $team->user_id = Auth::user()->id;
        $team->save();
        $user = Auth::user();
        $user->team = true;
        $user->update();

        return response()->json(['status'=>true,'message'=>'New team created for current user', 'data'=> []],StatusCodes::$created);
    }

    public function update(Request $request){
        $data = $request->all();
        $all = $data['attack'] + $data['midfield'] + $data['defense'] + $data['points'];

        $team = Auth::user()->user_team;
        $teamAll = $team->attack + $team->midfield + $team->defense + $team->points;

        if($all > $teamAll) return response()->json(['status'=>false, 'message'=> 'This data is not acceptable', 'data'=> $data], StatusCodes::$vErrors);

        $team = Auth::user()->user_team;
        $team->name = $data['name'];
        $team->attack = $data['attack'];
        $team->defense = $data['defense'];
        $team->midfield = $data['midfield'];
        $team->points = $data['points'];
        $team->update();

        return response()->json(['status'=>true,'message'=>'Team updated for current user', 'data'=> []],StatusCodes::$success);
    }

    public function uploadImage(Request $request){

        if($request->hasFile('image')){
            $image      = $request->file('image');
            $dbImage = Auth::user()->user_team->image;
            $fileName   = random_int(1,99999).'-'.time(). '.' . $image->getClientOriginalExtension();

            if($dbImage){
                Storage::disk('public')->delete('uploads/team/'.$dbImage->name);
                $dbImage->name = random_int(1,99999).'-'.time(). '.' . $image->getClientOriginalExtension();
                $dbImage->update();
                $fileName = $dbImage->name;
            }else {
                $img = new Image();
                $img->name = $fileName;
                $img->user_team_id = Auth::user()->user_team->id;
                $img->save();
            }

            Storage::disk('public')->put('uploads/team/'.$fileName, file_get_contents($image));

            return response()->json(['status'=> true, 'message'=> 'image saved', 'data'=> asset('storage/uploads/team/'.$fileName)], StatusCodes::$success);
        }else{
            return response()->json(['status'=> false, 'message'=> 'No image file present in request', 'data'=> null], StatusCodes::$vErrors);
        }
    }

    public function alltime(){
        $teams = UserTeam::where('id', '>', 1)
            ->orderBy('win_percent', 'DESC')
            ->orderBy('draw_percent', 'DESC')
            ->orderBy('goals', 'DESC')
            ->orderBy('concede', 'ASC')
            ->get();
        return response()->json(['status'=> true, 'message'=> 'All time ranking for user teams', 'data'=> $teams], StatusCodes::$success);
    }
}
