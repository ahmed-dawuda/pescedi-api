<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowed_domains = [
            'http://localhost:8081',
            'http://localhost:8080',
            'http://localhost:8082',
            'http://localhost:8083',
            'https://beta.pescedi.com'
        ];

        if (isset($request->server()['HTTP_ORIGIN'])) {
            $origin = $request->server()['HTTP_ORIGIN'];

            if (in_array($origin, $allowed_domains)) {
                return $next($request)->header('Access-Control-Allow-Origin', $origin)
                                        ->header('Access-Control-Allow-Credentials', 'true')
                                        ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
                                        ->header('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin, X-Requested-With, Content-Type, X-Auth-Key, Datatype, Authorization');
            }
        }

        return $next($request);
    }
}
