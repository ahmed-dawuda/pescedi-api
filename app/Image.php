<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function user_team(){
        return $this->belongsTo('App\UserTeam');
    }
}
