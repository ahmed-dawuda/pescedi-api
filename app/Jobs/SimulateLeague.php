<?php

namespace App\Jobs;

use App\GlobalHelpers\Leagues\LeagueSimulation;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SimulateLeague implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $league;
    protected $key;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($league, $key)
    {
        $this->league = $league;
        $this->key = $key;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $simulation = new LeagueSimulation($this->league, $this->key);
        $simulation->simulate();
    }
}
