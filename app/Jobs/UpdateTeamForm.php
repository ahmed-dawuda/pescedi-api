<?php

namespace App\Jobs;

use App\Form;
use App\TeamFixture;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateTeamForm implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $home;
    protected $away;
    protected $score;
    protected $bet;
    protected $won;
    protected $slip_id;

    public function __construct($home, $away, $score, $bet, $won, $slip_id)
    {
        $this->home = $home;
        $this->away = $away;
        $this->score = $score;
        $this->bet = $bet;
        $this->won = $won;
        $this->slip_id = $slip_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //updating home
        $this->home->matches += 1;
        $this->home->won += $this->score['home'] > $this->score['away'] ? 1 : 0;
        $this->home->loss += $this->score['home'] < $this->score['away'] ? 1 : 0;
        $this->home->goals += $this->score['home'];
        $this->home->concede += $this->score['away'];
        $this->home->win_percent = $this->home->won / $this->home->matches;
        $this->home->draw_percent = ($this->home->matches - $this->home->won - $this->home->loss) / $this->home->matches;
        $this->home->attack += $this->score['home'] * 0.01;

        //updating away
        $this->away->matches += 1;
        $this->away->won += $this->score['away'] > $this->score['home'] ? 1 : 0;
        $this->away->loss += $this->score['away'] < $this->score['home'] ? 1 : 0;
        $this->away->goals += $this->score['away'];
        $this->away->concede += $this->score['home'];
        $this->away->win_percent = $this->away->won / $this->away->matches;
        $this->away->draw_percent = ($this->away->matches - $this->away->won - $this->away->loss) / $this->away->matches;
        $this->away->attack += $this->score['away'] * 0.01;


        if($this->score['home'] > $this->score['away']){

            $this->home->midfield += 0.01;
            $this->home->defense += 0.01;

            //updating home form
            $form = new Form();
            $form->status = 'W';
            $form->team_id = $this->home->id;
            $form->save();

            //updating away form
            $f = new Form();
            $f->status = 'L';
            $f->team_id = $this->away->id;
            $f->save();
        }else if($this->score['away'] > $this->score['home']){

            $this->away->midfield += 0.01;
            $this->away->defense += 0.01;

            //updating home form
            $form = new Form();
            $form->status = 'L';
            $form->team_id = $this->home->id;
            $form->save();

            //updating away form
            $f = new Form();
            $f->status = 'W';
            $f->team_id = $this->away->id;
            $f->save();
        }else{
            $this->away->midfield += 0.01;
            $this->away->defense += 0.01;

            $this->home->midfield += 0.01;
            $this->home->defense += 0.01;

            //updating home form
            $form = new Form();
            $form->status = 'D';
            $form->team_id = $this->home->id;
            $form->save();

            //updating away form
            $f = new Form();
            $f->status = 'D';
            $f->team_id = $this->away->id;
            $f->save();
        }

        //saving changes
        $this->home->update();
        $this->away->update();

        //saving the fixture
        $fixture = new TeamFixture();
        $fixture->home_id = $this->home->id;
        $fixture->away_id = $this->away->id;
        $fixture->home_score = $this->score['home'];
        $fixture->away_score = $this->score['away'];
        $fixture->odd = $this->bet['odd'];
        $fixture->bet_type = $this->bet['type'];
        $fixture->status = $this->won;
        $fixture->slip_id = $this->slip_id;
        $fixture->save();
    }
}
