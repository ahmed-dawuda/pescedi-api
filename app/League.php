<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class League extends Model
{
    public function teams(){
        return $this->belongsToMany('App\UserTeam','participations');
    }

    public function countParticipants(){
        return $this->teams->count();
    }

    public function weeks(){
        return $this->hasMany('App\Week');
    }
}
