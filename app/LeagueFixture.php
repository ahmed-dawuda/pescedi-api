<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeagueFixture extends Model
{
    public function week(){
        return $this->belongsTo('App\Week');
    }
}
