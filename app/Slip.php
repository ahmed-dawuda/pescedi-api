<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slip extends Model
{
    public function fixtures(){
        return $this->hasMany('App\TeamFixture');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
