<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function forms(){
        return $this->hasMany('App\Form')->orderBy('created_at','DESC');
    }

}
