<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamFixture extends Model
{
    public function slip(){
        return $this->belongsTo('App\Slip');
    }
}
