<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','api_token','phone','team', 'fullname'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user_team(){
        return $this->hasOne('App\UserTeam');
    }
    
    public function account(){
        return $this->hasOne('App\Account');
    }

    public function slips(){
//        if($date == null){
            return $this->hasMany('App\Slip')->orderBy('created_at', 'DESC');
//        }else{
//            return $this->hasMany('App\Slip')->where('created_at',$date);
//        }

    }
    
    public function setup(){
        return $this->hasOne('App\Setup');
    }
    
}
