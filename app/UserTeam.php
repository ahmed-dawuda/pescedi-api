<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTeam extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function image(){
        return $this->hasOne('App\Image');
    }

    public function leagues(){
        return $this->belongsToMany('App\League', 'participations')->orderBy('expiry', 'ASC');
    }

    public function unJoinedLeaguesRepo(){
        $leagues_ids = $this->leagues->pluck('id');
        return League::whereNotIn('id',$leagues_ids)->where('completed', false)->orderBy('expiry','ASC');
    }

    public function upcomingLeagues($take = null){
        if($take != null){
            return $this->leagues->where('completed',false)->take($take);
        }
        return $this->leagues->where('completed',false);
    }

    public function completedLeagues($take = null){
        if($take != null){
            return $this->leagues->where('completed',true)->take($take);
        }
        return $this->leagues->where('completed',true);
    }
    
    public function table(){
        return $this->hasOne('App\LeagueTable');
    }
    
    public function wins(){
        return $this->hasMany('App\LeagueWinner');
    }
    
    public function trophies(){
        return $this->wins->count();
    }
}
