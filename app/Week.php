<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    public function league(){
        return $this->belongsTo('App\League');
    }

    public function leagueFixtures(){
        return $this->hasMany('App\LeagueFixture');
    }
}
