<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name')->unique();
            $table->string('description')->nullable();
            $table->double('attack');
            $table->double('midfield');
            $table->double('defense');
            $table->integer('matches');
            $table->integer('won');
            $table->integer('loss');
            $table->integer('goals');
            $table->integer('concede');
            $table->double('win_percent');
            $table->double('draw_percent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
