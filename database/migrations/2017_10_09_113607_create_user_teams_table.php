<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_teams', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name')->nullable();
//            $table->text('description')->nullable();
            $table->double('attack')->default(5);
            $table->double('midfield')->default(5);
            $table->double('defense')->default(5);
            $table->integer('matches')->default(0);
            $table->integer('won')->default(0);
            $table->integer('loss')->default(0);
            $table->integer('goals')->default(0);
            $table->integer('concede')->default(0);
            $table->double('win_percent')->default(0);
            $table->double('draw_percent')->default(0);
            $table->double('points')->default(85);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_teams');
    }
}
