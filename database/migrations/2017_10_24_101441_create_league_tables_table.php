<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeagueTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_team_id')->unsigned();
            $table->foreign('user_team_id')->references('id')->on('user_teams');
            $table->integer('league_id')->unsigned();
            $table->foreign('league_id')->references('id')->on('leagues');

            $table->integer('played')->default(0);
            $table->integer('won')->default(0);
            $table->integer('drawn')->default(0);
            $table->integer('loss')->default(0);
            $table->integer('g_f')->default(0);
            $table->integer('g_d')->default(0);
            $table->integer('g_a')->default(0);
            $table->integer('pts')->default(0);
            $table->double('earning')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('league_tables');
    }
}
