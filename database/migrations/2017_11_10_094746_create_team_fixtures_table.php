<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamFixturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_fixtures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('teams');

            $table->integer('away_id')->unsigned();
            $table->foreign('away_id')->references('id')->on('teams');

            $table->integer('home_score');
            $table->integer('away_score');

            $table->double('odd')->default(0);
            $table->string('bet_type');
            
            $table->boolean('status');

            $table->integer('slip_id')->unsigned();
            $table->foreign('slip_id')->references('id')->on('slips');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_fixtures');
    }
}
