<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(TeamTableSeeder::class);
        $this->call(FormTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AccountTableSeeder::class);
        $this->call(UserTeamTableSeeder::class);
//        $this->call(LeagueTableSeeder::class);
//        $this->call(ParticipationTableSeeder::class);
//        $this->call(StandingsTableSeeder::class);

    }
}
