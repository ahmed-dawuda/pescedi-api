<?php

use Illuminate\Database\Seeder;

class FormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //BARCELONA
        DB::table('forms')->insert([
            'team_id' => 1,
            'status' => 'W',
        ]);

        DB::table('forms')->insert([
            'team_id' => 1,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 1,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 1,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 1,
            'status' => 'W',
        ]);

        DB::table('forms')->insert([
            'team_id' => 1,
            'status' => 'L',
        ]);

        DB::table('forms')->insert([
            'team_id' => 1,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 1,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 1,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 1,
            'status' => 'D',
        ]);



        //REAL MADRID

        DB::table('forms')->insert([
            'team_id' => 2,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 2,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 2,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 2,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 2,
            'status' => 'W',
        ]);

        DB::table('forms')->insert([
            'team_id' => 2,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 2,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 2,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 2,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 2,
            'status' => 'L',
        ]);



        //CHELSEA
        DB::table('forms')->insert([
            'team_id' => 3,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 3,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 3,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 3,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 3,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 3,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 3,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 3,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 3,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 3,
            'status' => 'D',
        ]);



        //LIVERPOOL
        DB::table('forms')->insert([
            'team_id' => 4,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 4,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 4,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 4,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 4,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 4,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 4,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 4,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 4,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 4,
            'status' => 'D',
        ]);


        //PSG
        DB::table('forms')->insert([
            'team_id' => 5,
            'status' => 'W',
        ]);

        DB::table('forms')->insert([
            'team_id' => 5,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 5,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 5,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 5,
            'status' => 'W',
        ]);

        DB::table('forms')->insert([
            'team_id' => 5,
            'status' => 'L',
        ]);

        DB::table('forms')->insert([
            'team_id' => 5,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 5,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 5,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 5,
            'status' => 'D',
        ]);



        //BAYERN MUNICH

        DB::table('forms')->insert([
            'team_id' => 6,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 6,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 6,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 6,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 6,
            'status' => 'W',
        ]);

        DB::table('forms')->insert([
            'team_id' => 6,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 6,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 6,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 6,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 6,
            'status' => 'L',
        ]);



        //DORTMUND
        DB::table('forms')->insert([
            'team_id' => 7,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 7,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 7,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 7,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 7,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 7,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 7,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 7,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 7,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 7,
            'status' => 'D',
        ]);



        //INTER MILAN
        DB::table('forms')->insert([
            'team_id' => 8,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 8,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 8,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 8,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 8,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 8,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 8,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 8,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 8,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 8,
            'status' => 'D',
        ]);


        //SAO PAULO
        DB::table('forms')->insert([
            'team_id' => 9,
            'status' => 'W',
        ]);

        DB::table('forms')->insert([
            'team_id' => 9,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 9,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 9,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 9,
            'status' => 'W',
        ]);

        DB::table('forms')->insert([
            'team_id' => 9,
            'status' => 'L',
        ]);

        DB::table('forms')->insert([
            'team_id' => 9,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 9,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 9,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 9,
            'status' => 'D',
        ]);



        //ABERDEEN

        DB::table('forms')->insert([
            'team_id' => 10,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 10,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 10,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 10,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 10,
            'status' => 'W',
        ]);

        DB::table('forms')->insert([
            'team_id' => 10,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 10,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 10,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 10,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 10,
            'status' => 'L',
        ]);



        //OLYMPIC LYON
        DB::table('forms')->insert([
            'team_id' => 11,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 11,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 11,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 11,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 11,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 11,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 11,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 11,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 11,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 11,
            'status' => 'W',
        ]);



        //CELTIC
        DB::table('forms')->insert([
            'team_id' => 12,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 12,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 12,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 12,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 12,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 12,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 12,
            'status' => 'W',
        ]);
        DB::table('forms')->insert([
            'team_id' => 12,
            'status' => 'L',
        ]);
        DB::table('forms')->insert([
            'team_id' => 12,
            'status' => 'D',
        ]);
        DB::table('forms')->insert([
            'team_id' => 12,
            'status' => 'D',
        ]);


    }
}
