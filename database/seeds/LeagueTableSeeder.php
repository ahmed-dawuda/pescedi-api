<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LeagueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('leagues')->insert([
            'name' => 'Premier league',
            'description' => 'English Premier league',
            'owner' => 1,
            'entry_fee' => 56.12,
            'expiry' => Carbon::now()->addMinutes(2),
            'participants' => 4,
        ]);

        DB::table('leagues')->insert([
            'name' => 'Bundesliga',
            'description' => 'German League',
            'owner' => 2,
            'entry_fee' => 6.00,
            'expiry' => Carbon::now()->addMinutes(2),
            'participants' => 4,
        ]);

        DB::table('leagues')->insert([
            'name' => 'Laliga',
            'description' => 'Spanish League',
            'owner' => 2,
            'entry_fee' => 30.12,
            'expiry' => Carbon::now()->addMinutes(2),
            'participants' => 4,
        ]);

    }
}
