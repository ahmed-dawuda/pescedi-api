<?php

use Illuminate\Database\Seeder;

class StandingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('league_tables')->insert([
            'user_team_id' => 5,
            'league_id' => 1,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 5,
            'league_id' => 2,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 5,
            'league_id' => 3,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 2,
            'league_id' => 1,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 2,
            'league_id' => 2,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 2,
            'league_id' => 3,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 3,
            'league_id' => 1,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 3,
            'league_id' => 2,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 3,
            'league_id' => 3,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 4,
            'league_id' => 1,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 4,
            'league_id' => 2,
        ]);

        DB::table('league_tables')->insert([
            'user_team_id' => 4,
            'league_id' => 3,
        ]);


    }
}
