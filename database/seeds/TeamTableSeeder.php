<?php

use Illuminate\Database\Seeder;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            'name' => 'Barcelona',
            'description' => 'Spanish laliga',
            'attack' => 62.82,
            'midfield' => 50.92,
            'defense' => 46,
            'matches' => 10,
            'won' => 6,
            'loss' => 2,
            'goals' => 24,
            'concede' => 3,
            'win_percent' => 6/10,
            'draw_percent' => 2/10
        ]);

        DB::table('teams')->insert([
            'name' => 'R. Madrid',
            'description' => 'English Premiere league',
            'attack' => 58.23,
            'midfield' => 54.00,
            'defense' => 45.12,
            'matches' => 10,
            'won' => 5,
            'loss' => 3,
            'goals' => 14,
            'concede' => 2,
            'win_percent' => 5/10,
            'draw_percent' => 2/10
        ]);

        DB::table('teams')->insert([
            'name' => 'Chelsea',
            'description' => 'English Premier League',
            'attack' => 47,
            'midfield' => 43,
            'defense' => 40.43,
            'matches' => 10,
            'won' => 5,
            'loss' => 1,
            'goals' => 9,
            'concede' => 17,
            'win_percent' => 5/10,
            'draw_percent' => 4/10
        ]);

        DB::table('teams')->insert([
            'name' => 'Liverpool',
            'description' => 'English Premier League',
            'attack' => 40,
            'midfield' => 39.12,
            'defense' => 45,
            'matches' => 10,
            'won' => 3,
            'loss' => 3,
            'goals' => 4,
            'concede' => 31,
            'win_percent' => 3/10,
            'draw_percent' => 3/10
        ]);

        DB::table('teams')->insert([
            'name' => 'PSG',
            'description' => 'French ligue 1',
            'attack' => 61.82,
            'midfield' => 50.92,
            'defense' => 46,
            'matches' => 10,
            'won' => 6,
            'loss' => 2,
            'goals' => 24,
            'concede' => 3,
            'win_percent' => 6/10,
            'draw_percent' => 2/10
        ]);

        DB::table('teams')->insert([
            'name' => 'B. Munich',
            'description' => 'German Bundesliga',
            'attack' => 68.23,
            'midfield' => 54.00,
            'defense' => 45.12,
            'matches' => 10,
            'won' => 5,
            'loss' => 3,
            'goals' => 14,
            'concede' => 2,
            'win_percent' => 5/10,
            'draw_percent' => 2/10
        ]);

        DB::table('teams')->insert([
            'name' => 'B. Dortmund',
            'description' => 'German Bundesliga',
            'attack' => 47,
            'midfield' => 43,
            'defense' => 40.43,
            'matches' => 10,
            'won' => 5,
            'loss' => 1,
            'goals' => 9,
            'concede' => 17,
            'win_percent' => 5/10,
            'draw_percent' => 4/10
        ]);

        DB::table('teams')->insert([
            'name' => 'Inter Milan',
            'description' => 'Italian seria A',
            'attack' => 40,
            'midfield' => 39.12,
            'defense' => 45,
            'matches' => 10,
            'won' => 3,
            'loss' => 3,
            'goals' => 4,
            'concede' => 31,
            'win_percent' => 3/10,
            'draw_percent' => 3/10
        ]);

        DB::table('teams')->insert([
            'name' => 'Sao Paulo',
            'description' => 'Brazilian league ',
            'attack' => 45.82,
            'midfield' => 50.92,
            'defense' => 40,
            'matches' => 10,
            'won' => 6,
            'loss' => 2,
            'goals' => 24,
            'concede' => 3,
            'win_percent' => 6/10,
            'draw_percent' => 2/10
        ]);

        DB::table('teams')->insert([
            'name' => 'Aberdeen',
            'description' => 'Scottish league',
            'attack' => 48.23,
            'midfield' => 44.00,
            'defense' => 50.12,
            'matches' => 10,
            'won' => 5,
            'loss' => 3,
            'goals' => 14,
            'concede' => 2,
            'win_percent' => 5/10,
            'draw_percent' => 2/10
        ]);

        DB::table('teams')->insert([
            'name' => 'Olympic Lyon',
            'description' => 'French ligue 1',
            'attack' => 47,
            'midfield' => 43,
            'defense' => 40.43,
            'matches' => 10,
            'won' => 5,
            'loss' => 1,
            'goals' => 9,
            'concede' => 17,
            'win_percent' => 5/10,
            'draw_percent' => 4/10
        ]);

        DB::table('teams')->insert([
            'name' => 'Celtic',
            'description' => 'Scottish league',
            'attack' => 40,
            'midfield' => 39.12,
            'defense' => 45,
            'matches' => 10,
            'won' => 3,
            'loss' => 3,
            'goals' => 4,
            'concede' => 31,
            'win_percent' => 3/10,
            'draw_percent' => 3/10
        ]);

    }
}
