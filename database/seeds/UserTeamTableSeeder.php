<?php

use Illuminate\Database\Seeder;

class UserTeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_teams')->insert([
            'name' => 'Pescedi FC',
            'attack' => 40,
            'defense' => 25,
            'midfield' => 25,
            'user_id' => 1,
        ]);
//
//        DB::table('user_teams')->insert([
//            'name' => 'FC Kofi',
////            'description' => 'Smartest team',
//            'attack' => 50,
//            'defense' => 25,
//            'midfield' => 25,
//            'user_id' => 5,
//        ]);
//
//        DB::table('user_teams')->insert([
//            'name' => 'Knight FC',
////            'description' => 'Coolest team',
//            'attack' => 50,
//            'defense' => 25,
//            'midfield' => 25,
//            'user_id' => 2,
//        ]);
//
//        DB::table('user_teams')->insert([
//            'name' => 'Mighty Jets',
////            'description' => 'We the best',
//            'attack' => 50,
//            'defense' => 25,
//            'midfield' => 25,
//            'user_id' => 3,
//        ]);
//
//        DB::table('user_teams')->insert([
//            'name' => 'Berlin FC',
////            'description' => 'Champions',
//            'attack' => 50,
//            'defense' => 25,
//            'midfield' => 25,
//            'user_id' => 4,
//        ]);
    }
}
