<?php

use App\GlobalHelpers\GenerateToken;
use Illuminate\Database\Seeder;
//use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'pescedi',
            'email' => 'pescedi@pescedi.com',
            'phone' => '0547406827',
            'fullname' => 'Pescedi Pescedi',
            'api_token' => GenerateToken::getToken(64),
            'team' => true,
            'password' => bcrypt('@pescedi-2017'),
            'profile' => true
        ]);
        
//        DB::table('users')->insert([
//            'username' => 'kofi',
//            'email' => 'kofi@kofi.com',
//            'phone' => '0547406828',
//            'fullname' => 'Ahmed Dawuda',
//            'api_token' => GenerateToken::getToken(64),
//            'team' => true,
//            'password' => bcrypt('123456'),
//            'profile' => true
//        ]);
//
//        DB::table('users')->insert([
//            'username' => 'Abena',
//            'email' => 'abena@abena.com',
//            'phone' => '0541041580',
//            'fullname' => 'Abena Tiwaa',
//            'api_token' => GenerateToken::getToken(64),
//            'team' => true,
//            'password' => bcrypt('123456'),
//            'profile' => true
//        ]);
//
//        DB::table('users')->insert([
//            'username' => 'rehi',
//            'email' => 'rehi@rehi.com',
//            'phone' => '0246230993',
//            'fullname' => 'Rehi Amartey',
//            'api_token' => GenerateToken::getToken(64),
//            'team' => true,
//            'password' => bcrypt('123456'),
//            'profile' => true
//        ]);
//
//        DB::table('users')->insert([
//            'username' => 'kojo',
//            'email' => 'kojo@kojo.com',
//            'phone' => '0549933011',
//            'fullname' => 'Kojo Kojo',
//            'api_token' => GenerateToken::getToken(64),
//            'team' => true,
//            'password' => bcrypt('123456'),
//            'profile' => true
//        ]);
    }
}
