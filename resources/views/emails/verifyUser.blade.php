<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
    <style>
        .wrapper{width:60%; margin:5% auto; box-shadow:0 0 2px #aaa; font-family:Hind;}
        .logo_header{width:100%; height:70px;background:#1CA8DD; padding:10px;}
        .email_body{width:100%; padding:100px;}
    </style>
</head>
<body>

<div class="container">
    <div class="row">

        <div class="wrapper">
            <div class="logo_header">
               <h3 style="font-size: 2em; color: white;">PESCEDI</h3>
            </div>
            <div class="email_body">
                <h1 class="text-center">Thankyou for you joining us</h1>
                <p>
                    SeeDocOnline makes seeing a doctor simple. Our interactive healthcare
                    services allow patients to speak to a doctor whenever and wherever they want. We have a
                    team verified doctors from around the globe, making it possible speaking in your native language.
                </p>
            </div>
        </div>

    </div>
</div>
</body>
</html>