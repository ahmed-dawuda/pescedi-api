<?php

use App\GlobalHelpers\Leagues\LeagueSimulation;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/weeks', function(){
   $sim = new \App\GlobalHelpers\Leagues\MatchDaySimulation();
    $leagues = $sim->leagues_weekly_Matches();
    foreach($leagues as $key => $league){
        $simulation = new LeagueSimulation($league, $key);
        $simulation->simulate();
    }
});

Route::get('/ahmed', function(){
   return "working";
});

//get the details for home page
Route::get('/home', 'HomeController@index');
//register a new user
Route::post('/register','RegisterController@register');
//login in an exisiting user
Route::post('/login','LoginController@login');//in use
//getting fixtures to be used
Route::get('/fixtures', 'FixtureController@fixtures');//in use
//simulate a bet slip
Route::middleware('auth:api')->post('/simulate','BetslipController@simulate');//in use

Route::middleware('auth:api')->get('/table', 'FixtureController@table');


Route::middleware('auth:api')->put('user/profile','ProfileController@update');

//create new team's profile
Route::middleware('auth:api')->post('/user/team', 'TeamController@create');//in use
//update user team's profile
Route::middleware('auth:api')->put('/user/team', 'TeamController@update');//in use
//create or update user team's logo
Route::middleware('auth:api')->post('/user/team/image', 'TeamController@uploadImage');//in use
//get user team details
Route::middleware('auth:api')->get('/user/team', 'TeamController@myTeam'); //in use
//get user bet slips
Route::middleware('auth:api')->get('/user/slips', 'BetslipController@slips'); //in use
//create new league
Route::middleware('auth:api')->post('/user/league/create', 'LeagueController@create');
//join a given league
Route::middleware('auth:api')->post('/user/league/join', 'LeagueController@join');
//get user team's upcoming leagues
Route::middleware('auth:api')->get('/user/league/pending', 'LeagueController@upcoming');//in use
//get latest unjoined leagues
Route::middleware('auth:api')->get('/user/league/latest', 'LeagueController@latest');

Route::middleware('auth:api')->get('/user/league/next-prev/{current_id}', 'LeagueController@nextPrev');

Route::get('/user/team/all-time-ranking', 'TeamController@alltime');

Route::middleware('auth:api')->post('/user/team/bot/settings', 'SetupController@settings');

//Route::middleware('auth:api')->put('/user/team/bot/settings', 'SetupController@update');

Route::middleware('auth:api')->put('/user/team/bot/settings/turn', 'SetupController@turn');


//get user team's completed leagues
Route::middleware('auth:api')->get('/user/league/completed', 'LeagueController@completed');//in use

//
////get both completed and upcoming leagues
//Route::middleware('auth:api')->get('/user/leagues', 'LeagueController@leagues');//in use, to be changed
//leave a given league
Route::middleware('auth:api')->post('/user/league/leave', 'LeagueController@leave');//in use
//get the participants of a particular league, but this url is deprecated and replaced by a different one
Route::middleware('auth:api')->get('/user/league/participants/{id}', 'LeagueController@participants');//in use



//get fixtures of completed leagues group by weeks
Route::middleware('auth:api')->get('/user/league/fixtures/{league_id}', 'LeagueResultController@fixtures');
//get table results of completed leagues for a given league
Route::middleware('auth:api')->get('/user/league/table/{league_id}', 'LeagueResultController@table');
//get members of a given league
Route::middleware('auth:api')->get('/user/league/members/{league_id}', 'LeagueResultController@members');

Route::middleware('auth:api')->get('/user/league/all/{league_id}', 'LeagueResultController@results');

Route::middleware('auth:api')->get('/user/account', 'AccountController@details');


