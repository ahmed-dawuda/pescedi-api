<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/weeks', function () {
    
    $simulation = new \App\GlobalHelpers\Leagues\MatchDaySimulation();
    $leagues = $simulation->leagues_weekly_Matches();
//    return $leagues;
    foreach($leagues as $key => $league){
        $sim = new \App\GlobalHelpers\Leagues\LeagueSimulation($league, $key);
        $sim->simulate();
    }
    return "simulation complete";
});


